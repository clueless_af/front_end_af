import React, {Component} from 'react'
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'

import Home from '../Home'
import Courses from '../Courses'
import Instructors from '../Instructors'
import ViewInstructor from '../ViewInstructor'
import ViewCourse from '../ViewCourse'
import editInstructor from '../editInstructor.component'
import Admins from '../Admins'
import ViewAdmins from '../ViewAdmins'

class Navbar extends Component{
    render(){
        return(
            <>
                <Router>
                    <div className="container">
                        <nav className="navbar navbar-expand-lg nav justify-content-center navbar-light bg-secondary text-white">
                            <Link to="/" className="navbar-brand">Admin Page</Link>
                            <div className="collpase nav-collapse">
                                <ul className="navbar-nav mr-auto">
                                    <li className="navbar-item">
                                        <Link to="/" className="nav-link">Home</Link>
                                    </li>
                                    <li className="navbar-item">
                                        <Link to="/admins" className="nav-link">Admins</Link>
                                    </li>
                                    <li className="navbar-item">
                                        <Link to="/instructors" className="nav-link">Instructors</Link>
                                    </li>
                                    <li className="navbar-item">
                                        <Link to="/courses" className="nav-link">Courses</Link>
                                    </li>
                                    <li className="navbar-item">
                                        <Link to="/viewadmins" className="nav-link">View Admins</Link>
                                    </li>
                                    <li className="navbar-item">
                                        <Link to="/viewinstructors" className="nav-link">View Instructors</Link>
                                    </li>
                                    <li className="navbar-item">
                                        <Link to="/viewcourse" className="nav-link">View Course</Link>
                                    </li>
                                </ul>
                            </div>
                        </nav>

                        <Route exact path="/" component={Home}/>
                        <Route path="/courses" component={Courses}/>
                        <Route path="/instructors" component={Instructors}/>
                        <Route path="/viewinstructors" component={ViewInstructor}/>
                        <Route path="/viewcourse" component={ViewCourse}/>
                        <Route path="/editI" component={editInstructor}/>
                        <Route path="/admins" component={Admins}/>
                        <Route path="/viewadmins" component={ViewAdmins}/>
                    </div>
                </Router>
            </>
        )
    }
}

export default Navbar