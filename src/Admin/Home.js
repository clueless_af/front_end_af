import React, {Component} from 'react'

class Home extends Component {
    render() {
        return (
            <div style={{marginTop: 10, marginBottom: 100}}>
                <div class="card">
                    <h3 class="card-header">Admin</h3>
                    <div class="card-body">
                        <h4 class="card-title">Create Admin</h4>
                        <p class="card-text">An admin is a user who can create other Admins, create Instructors and create Courses.</p>
                        <a href="#!" class="btn btn-primary">Admin</a>
                    </div>
                    <h3 class="card-header">Instructor</h3>
                    <div class="card-body">
                        <h4 class="card-title">Create Instructor</h4>
                        <p class="card-text">Admin can create an Instructor. Once an Instructor is created, a mail will be sent to the Instructor's mail.</p>
                        <a href="#!" class="btn btn-primary">Instructor</a>
                    </div>
                    <h3 class="card-header">Courses</h3>
                    <div class="card-body">
                        <h4 class="card-title">Create Course</h4>
                        <p class="card-text">Admin can create Courses. Once a Course is created, the assigned Instructor will get a notification about the creation of the Course.</p>
                        <a href="#!" class="btn btn-primary">Courses</a>
                    </div>
                </div>
            </div>
        )
    }
    
}

export default Home