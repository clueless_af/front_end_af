import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class ViewCourse extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        fetch('http://localhost:3002/courses').then(res => {
            if (res.ok) {
                return res.json()
            }
        }).then(data => {
            var options = data.map((data) => {
                //const id = data._id
                return <tr>
                    <td>{data.cName}</td>
                    <td>{data.cCode}</td>
                    <td>{data.duration}</td>
                    <td>{JSON.stringify(data.instructors)}</td>
                    <td>
                        <Link to={"/edit/"+data._id}>Edit</Link>
                    </td>
                </tr>
            })
            this.setState({options: options})
        })
    }

    render(){
        return(
            <div style={{marginTop: 20}}>
                <h2>Courses</h2>
                <table className="table table-striped">
                    <tr>
                        <td>Course Name</td>
                        <td>Course Code</td>
                        <td>Course Duration</td>
                        <td>Instructors</td>
                        <td>Actions</td>
                    </tr>
                    {this.state.options}
                </table>
            </div>
        )
    }
}

export default ViewCourse