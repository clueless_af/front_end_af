import React, {Component} from 'react'

class Instructors extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            nic: "",
            birthday: "",
            email: "",
            mobileNo: "",
            password: ""
        }
        this.baseState=this.state
        this.onChangeName = this.onChangeName.bind(this)
        this.onChangeNic = this.onChangeNic.bind(this)
        this.onChangeBday = this.onChangeBday.bind(this)
        this.onChangeEmail = this.onChangeEmail.bind(this)
        this.onChangeMobile = this.onChangeMobile.bind(this)
        this.onChangePassword = this.onChangePassword.bind(this)
        this.handleInstructorSubmit = this.handleInstructorSubmit.bind(this)
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        })
    }

    onChangeNic(e) {
        this.setState({
            nic: e.target.value
        })
    }

    onChangeBday(e) {
        this.setState({
            birthday: e.target.value
        })
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    onChangeMobile(e) {
        this.setState({
            mobileNo: e.target.value
        })
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleInstructorSubmit(event) {
        event.preventDefault()
        event.stopPropagation()
        const instructor = {
            name: this.state.name,
            nic: this.state.nic,
            birthday: this.state.birthday,
            email: this.state.email,
            mobileNo: this.state.mobileNo,
            password: this.state.password
        }
        if (instructor.name && instructor.nic && instructor.birthday && instructor.email && instructor.mobileNo && instructor.password){
            fetch('http://localhost:3002/instructors',{
                method:'POST',
                body:JSON.stringify(instructor),
                headers:{"Content-Type":"application/json"}
            }).then(res => {
                if (res.ok) {
                    alert("Instructor Added!!!")
                    this.setState({...this.baseState})
                }
            }).catch(e=>{
                alert("Server Error!!!")
            })
        }else{
            alert("Required fields empty!!!")
        }
    }

    

    render() {
        return (
            <div>

                    <h2>Add New Instructor</h2>
                    <form onSubmit={this.handleInstructorSubmit}>
                    <div className="form-group">
                            <lable>Instructor Name:</lable>
                            <input type="text" className="form-control" value={this.state.name} 
                                onChange={this.onChangeName}/>
                        </div>
                        <div className="form-group">
                            <lable>Instructor NIC:</lable>
                            <input type="text" className="form-control" value={this.state.nic} 
                                onChange={this.onChangeNic}/>
                        </div>
                        <div className="form-group">
                            <lable>Birth Date:</lable>
                            <input type="text" className="form-control" value={this.state.birthday} 
                                onChange={this.onChangeBday}/>
                        </div>
                        <div className="form-group">
                            <lable>Email:</lable>
                            <input type="text" className="form-control" value={this.state.email} 
                                onChange={this.onChangeEmail}/>
                        </div>
                        <div className="form-group">
                            <lable>Instructor Mobile Number:</lable>
                            <input type="text" className="form-control" value={this.state.mobileNo} 
                                onChange={this.onChangeMobile}/>
                        </div>
                        <div className="form-group">
                            <lable>Password:</lable>
                            <input type="text" className="form-control" value={this.state.password} 
                                onChange={this.onChangePassword}/>
                        </div>

                        <br/>
                        <div className="form-group">
                            <input type="submit" value="Add Instructor" className="btn btn-primary"/> 
                        </div>                    
                        
                    </form>
            </div>
        )
    }
}

export default Instructors