import React, {Component} from 'react'

class Admins extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            nic: "",
            birthday: "",
            email: "",
            mobileNo: "",
            pasword: ""
        }
        this.baseState=this.state
        this.onChangeName = this.onChangeName.bind(this)
        this.onChangeNic = this.onChangeNic.bind(this)
        this.onChangeBday = this.onChangeBday.bind(this)
        this.onChangeEmail = this.onChangeEmail.bind(this)
        this.onChangeMobile = this.onChangeMobile.bind(this)
        this.onChangePassword = this.onChangePassword.bind(this)
        this.handleAdminSubmit = this.handleAdminSubmit.bind(this)
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        })
    }

    onChangeNic(e) {
        this.setState({
            nic: e.target.value
        })
    }

    onChangeBday(e) {
        this.setState({
            birthday: e.target.value
        })
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    onChangeMobile(e) {
        this.setState({
            mobileNo: e.target.value
        })
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        })
    }

    handleAdminSubmit(event) {
        event.preventDefault()
        event.stopPropagation()
        const admin = {
            name: this.state.name,
            nic: this.state.nic,
            birthday: this.state.birthday,
            email: this.state.email,
            mobileNo: this.state.mobileNo,
            pasword: this.state.password
        }
        if (admin.name && admin.nic && admin.birthday && admin.email && admin.mobileNo && admin.pasword){
            fetch('http://localhost:3001/admins',{
                method:'POST',
                body:JSON.stringify(admin),
                headers:{"Content-Type":"application/json"}
            }).then(res => {
                if (res.ok) {
                    alert("Admin Added!!!")
                    this.setState({...this.baseState})
                }
            }).catch(e=>{
                alert("Server Error!!!")
            })
        }else{
            alert("Required fields empty!!!")
        }
    }

    

    render() {
        return (
            <div>
                    <h2>Add New Admin</h2>
                    <form onSubmit={this.handleAdminSubmit}>
                        <div className="form-group">
                            <lable>Admin Name:</lable>
                            <input type="text" className="form-control" value={this.state.name} 
                                onChange={this.onChangeName}/>
                        </div>
                        <div className="form-group">
                            <lable>Admin NIC:</lable>
                            <input type="text" className="form-control" value={this.state.code} 
                                onChange={this.onChangeNic}/>
                        </div>
                        <div className="form-group">
                            <lable>Birth Date:</lable>
                            <input type="text" className="form-control" value={this.state.email} 
                                onChange={this.onChangeBday}/>
                        </div>
                        <div className="form-group">
                            <lable>Email:</lable>
                            <input type="text" className="form-control" value={this.state.name} 
                                onChange={this.onChangeEmail}/>
                        </div>
                        <div className="form-group">
                            <lable>Admin Mobile Number:</lable>
                            <input type="text" className="form-control" value={this.state.code} 
                                onChange={this.onChangeMobile}/>
                        </div>
                        <div className="form-group">
                            <lable>Password:</lable>
                            <input type="text" className="form-control" value={this.state.email} 
                                onChange={this.onChangePassword}/>
                        </div>

                        <br/>
                        <div className="form-group">
                            <input type="submit" value="Add Admin" className="btn btn-primary"/> 
                        </div>                    
                        
                    </form>
            </div>
        )
    }
}

export default Admins