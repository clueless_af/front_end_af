import React, {Component} from 'react'
import axios from 'axios'

class editInstructor extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            code: "",
            email: ""
        }
        this.baseState=this.state
        this.onChangeName = this.onChangeName.bind(this)
        this.onChangeCode = this.onChangeCode.bind(this)
        this.onChangeEmail = this.onChangeEmail.bind(this)
        this.handleInstructorSubmit = this.handleInstructorSubmit.bind(this)
    }

    componentDidMount() {
        axios.get('http://localhost3001/instructors/'+this.props.match.params.id)
            .then(response => {
                this.setState({
                    name: response.data.name,
                    code: response.data.code,
                    email: response.data.email
                })
            }).catch(err => {
                console.log(err)
            })
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        })
    }

    onChangeCode(e) {
        this.setState({
            code: e.target.value
        })
    }

    onChangeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    handleInstructorSubmit(event) {
        event.preventDefault()
        event.stopPropagation()
        const instructor = {
            name: this.state.name,
            code: this.state.code,
            email: this.state.email
        }
        if (instructor.name && instructor.code && instructor.email){
            axios.put('http://localhost:3001/instructors/'+this.props.match.params.id, instructor).
                then(res => {
                    if (res.ok) {
                            alert("Instructor Updated!!!")
                            this.setState({...this.baseState})
                        }
                    }).catch(e=>{
                        alert("Server Error!!!")
                    })
        }else{
            alert("Required fields empty!!!")
        }
    }

    render() {
        return (
            <div style={{marginTop: 20}}>
                <h3>Update</h3>
                <hr/>
                <form onSubmit={this.handleInstructorSubmit}>
                    <div className="form-group">
                        <lable>Instructor Name:</lable>
                        <input type="text" className="form-control" value={this.state.name} 
                            onChange={this.onChangeName}/>
                    </div>
                    <div className="form-group">
                        <lable>Instructor Code:</lable>
                        <input type="text" className="form-control" value={this.state.code} 
                            onChange={this.onChangeCode}/>
                    </div>
                    <div className="form-group">
                        <lable>Email:</lable>
                        <input type="text" className="form-control" value={this.state.email} 
                            onChange={this.onChangeEmail}/>
                    </div>
                    <br/>
                    <div className="form-group">
                        <input type="submit" value="Update Instructor" className="btn btn-primary"/>                     
                    </div>
                </form>
            </div>
        )
    }
}

export default editInstructor