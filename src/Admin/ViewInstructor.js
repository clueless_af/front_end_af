import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Axios from 'axios';

class ViewInstructor extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        fetch('http://localhost:3002/instructors').then(res => {
            if (res.ok) {
                return res.json()
            }
        }).then(data => {
            var options = data.data.map((data) => {
                const id = data._id
                return <tr>
                    <td>{data.name}</td>
                    <td>{data.nic}</td>
                    <td>{data.birthday}</td>
                    <td>{data.email}</td>
                    <td>{data.mobileNo}</td>
                    <td>{data.courses}</td>
                    <td>{data.password}</td>
                    <td>
                        <div>
                            <Link to={"/editI/"+data._id}>Edit</Link>
                        </div>
                    </td>
                </tr>
            })
            this.setState({options: options})
        })
    }

    render(){
        return(
            <div style={{marginTop: 20}}>
                <h2>Instructors</h2>
                <table className="table table-striped">
                    <tr>
                    <td>Instructor Name</td>
                    <td>Instructor NIC</td>
                    <td>Birth Date</td>
                    <td>Emaile</td>
                    <td>Mobile Number</td>
                    <td>Course</td>
                    <td>Password</td>
                    <td>Action</td>
                    </tr>
                    {this.state.options}
                </table>
            </div>
        )
    }
}

export default ViewInstructor