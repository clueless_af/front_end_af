import React, {Component} from 'react'

class Courses extends Component {

    constructor(props) {
        super(props)
        this.state = {
            cName: "",
            cCode: "",
            duration: "",
            instructors: [],
            instructorsDisplay: [],
            instructor: "",
            course: ""
        }
        this.baseState=this.state
        this.handleCourseSubmit = this.handleCourseSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.onChangeName = this.onChangeName.bind(this)
        this.onChangeCode = this.onChangeCode.bind(this)
        this.onChangeDuration = this.onChangeDuration.bind(this)
    }

    handleChange(event) {
        const {name, value} = event.target
        if (name == 'instructors') {
            const id = event.target[event.target.selectedIndex].id
            this.setState({instructorsDisplay: [...this.state.instructorsDisplay, value]})
            this.setState({instructors: [...this.state.instructors, id]})
        } else {
            this.setState({[name]: value})
        }
    }

    onChangeName(e) {
        this.setState({
            cName: e.target.value
        })
    }

    onChangeCode(e) {
        this.setState({
            cCode: e.target.value
        })
    }

    onChangeDuration(e) {
        this.setState({
            duration: e.target.value
        })
    }


    handleCourseSubmit(event) {
        event.preventDefault()
        event.stopPropagation()
        const course = {
            cName: this.state.cName,
            cCode: this.state.cCode,
            duration: this.state.duration,
            instructors: this.state.instructors
        }
        if (course.cName && course.cCode && course.duration && course.instructors){
            fetch('http://localhost:3002/courses',{
                method:'POST',
                body:JSON.stringify(course),
                headers:{"Content-Type":"application/json"}
            }).then(res => {
                if (res.ok) {
                    alert("New Course Added!!!")
                    this.setState({...this.baseState})
                }
            }).catch(e=>{
                alert("Server Error!!!")
            })
        }else{
            alert("Required fields empty!!!")
        }
    }

    componentDidMount() {
        fetch('http://localhost:3002/instructors').then(res => {
            if (res.ok) {
                return res.json()
            }
        }).then(data => {
            var options = data.data.map((data, id) => {
                return <option id={data.name} value={data.name}>{data.name}</option>
            })
            this.setState({options: options})
        })
    }

    render() {
        return (
                <div>
                    <h2>Add New Course</h2>
                    <form onSubmit={this.handleCourseSubmit}>
                        <div className="form-group">
                            <lable>Course Name:</lable>
                            <input type="text" className="form-control" value={this.state.cName} 
                                onChange={this.onChangeName}/>
                        </div>
                        <div className="form-group">
                            <lable>Course Code:</lable>
                            <input type="text" className="form-control" value={this.state.cCode} 
                                onChange={this.onChangeCode}/>
                        </div>
                        <div className="form-group">
                            <lable>Course Duration:</lable>
                            <input type="text" className="form-control" value={this.state.duration} 
                                onChange={this.onChangeDuration}/>
                        </div>
                        <div className="form-group">
                            <lable>Instructors Incharge:</lable>
                            <select type="select" name="instructors" className="form-control" value={this.state.instructor}
                                    options={[1, 2, 3, 4, 5]}
                                    onChange={this.handleChange}>
                                <option id={0} value="0"></option>
                                {this.state.options}
                            </select>
                        <p>{JSON.stringify(this.state.instructorsDisplay)}</p>
                        </div>
                        
                        <div className="form-group">
                            <input type="submit" value="Add Course" className="btn btn-primary"/> 
                        </div>
                    </form>
            </div>
        )
    }
}

export default Courses