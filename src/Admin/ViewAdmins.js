import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Axios from 'axios';

class ViewAdmins extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        fetch('http://localhost:3001/admins').then(res => {
            if (res.ok) {
                return res.json()
            }
        }).then(data => {
            var options = data.map((data) => {
                const id = data._id
                return <tr>
                    <td>{data.name}</td>
                    <td>{data.nic}</td>
                    <td>{data.birthday}</td>
                    <td>{data.email}</td>
                    <td>{data.mobileNo}</td>
                    <td>{data.password}</td>
                    <td>
                        <div>
                            <Link to={"/editI/"+data._id}>Edit</Link>
                        </div>
                    </td>
                </tr>
            })
            this.setState({options: options})
        })
    }

    render(){
        return(
            <div style={{marginTop: 20}}>
                <h2>Admins</h2>
                <table className="table table-striped" >
                    <tr>
                        <td>Admin Name</td>
                        <td>Admin NIC</td>
                        <td>Birth Date</td>
                        <td>Emaile</td>
                        <td>Mobile Number</td>
                        <td>Password</td>
                        <td>Action</td>
                    </tr>
                    {this.state.options}
                </table>
            </div>
        )
    }
}

export default ViewAdmins