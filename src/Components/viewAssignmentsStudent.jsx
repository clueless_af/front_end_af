import React, {Component} from 'react';
import '../StyleSheets/viewAssignments.css'
import Axios from "axios";

export default class ViewAssignmentsStudentComponent extends Component {
    constructor() {
        super();
        this.state = {
            assignments: [],
            nic:''
        }
    }

    componentDidMount() {
        var user = localStorage.getItem('sis-user')
        if (user) {
            user = JSON.parse(user)
            this.setState({
                nic:user.data.nic
            })
        }
        Axios.get('http://localhost:3002/assignments').then(data => {
            this.setState({
                assignments: data.data
            })
            console.log(data);
        })
    }

    render() {
        return (
            <div>
                <h2 style={{marginTop: 20}}>Assignments</h2>
                <table className="table table-striped" style={{marginTop: 20}}>
                    <thead>
                    <th>Course Name</th>
                    <th>Assignment Name</th>
                    <th>Description</th>
                    <th>Due Date</th>
                    </thead>
                    <tbody>
                    {
                        this.state.assignments.map(assignment=>{
                            return <tr key={assignment._id}>
                                <td>{assignment.coursename}</td>
                                <td>{assignment.assignmentname}</td>
                                <td>{assignment.description}</td>
                                <td>{assignment.due_dates}</td>
                            </tr>
                        })

                    }
                    </tbody>
                </table>
            </div>
        );
    }
}