import React, {Component} from 'react';
import '../StyleSheets/addAssignment.css'

export default class AssignmentSubmit extends Component {
    render() {
        return (
            <div  style={{marginTop: 0}}>

                <h3 style={{marginTop:0}} className="text-primary">Add Submission</h3>
                <form className="assignmentForm">
                    <div className="form-group">
                        <label>Course Name</label>
                        <input type="text"
                               className="form-control"
                               name="coursename"
                        />
                    </div>

                    <div className="form-group">
                        <label>Assignment Name</label>
                        <input type="text"
                               className="form-control"
                               name="assignmentname"
                        />
                    </div>

                    <div className="form-group">
                        <label>NIC</label>
                        <input type="text"
                               className="form-control"
                               name="nic"
                        />
                    </div>

                    <div className="form-group">
                        <label htmlFor="exampleFormControlFile1"> </label>
                        <input type="file" className="form-control-file" id="exampleFormControlFile1"/>
                    </div>
                    <div className="form-group">
                        <input type="submit" value="SUBMIT" className="btn btn-primary"/>
                    </div>

                </form>
            </div>
    );
    }
    }