import React, {Component} from 'react';
import '../StyleSheets/viewAssignments.css'
import Axios from 'axios';

export default class ViewCoursesComponent extends Component{
    constructor() {
        super();
        this.state = {
            courses: [],
            nic:''
        }
    }
    componentDidMount() {
        var user = localStorage.getItem('sis-user');
        if (user) {
            user = JSON.parse(user);
            this.setState({
                nic:user.data.nic
            })
        }
        Axios.get('http://localhost:3002/students/'+ user.data.nic).then(data => {
            this.setState({
                courses: data.data.data
            })
            console.log(data);
        })
    }
    render() {
        return(
            <div>
                <h2 style={{marginTop:20}}>Courses</h2>
                <table className="table table-striped" style={{marginTop:20}}>
                    <thead>
                    </thead>
                    <tbody>
                    {
                        this.state.courses.map(sub=>{
                            return <tr key={sub.nic}>
                                <td>
                                    <ul>
                                        <li>{sub.courses[0]}</li>
                                        <li>{sub.courses[1]}</li>
                                        <li>{sub.courses[5]}</li>
                                    </ul>
                                </td>

                            </tr>
                        })

                    }
                    </tbody>
                </table>
            </div>
        );
    }
}