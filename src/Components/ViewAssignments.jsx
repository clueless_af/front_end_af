import React, {Component} from 'react';
import '../StyleSheets/viewAssignments.css'
import Axios from "axios";
import ReactDOM from "react-dom";
import StudentHome from "./login";
import Login from "./instructorHome";
import AddMarks from "./addMarks";

export default class ViewAssignmentsComponent extends Component {
    constructor() {
        super();
        this.state = {
            assignments: [],
            nic: '',
            coursename: '',
            assignmentname: '',
            description: '',
            due_dates: ''
        }
    }

    componentDidMount() {
        var usr = localStorage.getItem('sis-user');
        if (usr) {
            usr = JSON.parse(usr)
            this.setState({
                nic: usr.data.nic
            })
        }
        Axios.get('http://localhost:3002/assignments/course/952881825v').then(response => {
            this.setState({
                assignments: response.data
            });
            console.log("jsfhg" + response);
        })
    }
    toAddMarks = () => {
        ReactDOM.render(<AddMarks/>, document.getElementById('root'));
    };
    editDueDates = (data) => {
        localStorage.setItem('current-assign', JSON.stringify(data));
        ReactDOM.render(<StudentHome/>, document.getElementById('root'));
    };

    render() {
        return (
            <div>
                <h2 style={{marginTop: 20}}>Assignments</h2>
                <table className="table table-striped" style={{marginTop: 20}}>
                    <thead>
                    <th>Course Name</th>
                    <th>Assignment Name</th>
                    <th>Description</th>
                    <th>Due Date</th>
                    </thead>
                    <tbody>
                    {
                        this.state.assignments.map(assignment => {
                            return <tr key={assignment._id}>
                                <td>{assignment.coursename}</td>
                                <td>{assignment.assignmentname}</td>
                                <td>{assignment.description}</td>
                                <td>{assignment.due_dates}</td>
                                <td>
                                    <button className="btn btn-info">Edit</button>
                                </td>
                            </tr>
                        })

                    }
                    </tbody>
                </table>
            </div>
        );
    }
}