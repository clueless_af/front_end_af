import React, {Component} from 'react';
import '../StyleSheets/viewAssignments.css'
import {Link,Router,Route,Switch} from "react-router-dom";
import Login from "./login";
import SignUp from "./signUp";

export default class MarksComponent extends Component{
    render() {
        return(
            <div>
                <h2 style={{marginTop:20}}>Assignments Marks</h2>
                <table className="table table-striped" style={{marginTop:20}}>
                    <thead>
                    <th>Student Id</th>
                    <th>Marks</th>
                    </thead>
                    <tbody>
                    {
                        // this.state.subjects.map(sub=>{
                        //     return <tr key={sub._id}>
                        //         <td>{sub.name}</td>
                        //         <td>{sub.description}</td>
                        //         <td>{sub.amount}</td>
                        //     </tr>
                        // })
                        <tr>
                            <td>IT 1618800</td>
                            <td>50</td>
                        </tr>

                    }
                    </tbody>
                </table>
            </div>
        );
    }
}