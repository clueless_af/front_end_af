import React, {Component} from 'react';
import '../StyleSheets/viewAssignments.css'
import axios from "axios";

export default class NotificationsStudentComponent extends Component{
    constructor() {
        super();
        this.state = {
            courses: [],
            nic:''
        }
    }
    componentDidMount() {
        var user = localStorage.getItem('sis-user')
        if (user) {
            user = JSON.parse(user)
            this.setState({
                nic:user.data.nic
            })
        }
        axios.get('http://localhost:3002/notifications/').then(data => {
            this.setState({
                courses: data.data
            })
            console.log(data);
        })
    }
    render() {
        return(
            <div>
                <h2 style={{marginTop:20}}>Notifications</h2>
                <table className="table table-striped" style={{marginTop:20}}>
                    <thead>
                    </thead>
                    <tbody>
                    {
                        this.state.courses.map(sub=>{
                            return <tr key={sub._id}>
                                <td>
                                    {sub.coursename}
                                    <br/>{sub.description}
                                </td>
                            </tr>
                        })

                    }
                    </tbody>
                </table>
            </div>
        );
    }
}