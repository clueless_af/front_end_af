import React, {Component} from 'react';
import '../StyleSheets/viewAssignments.css'
import axios from "axios";

export default class NotificationsComponent extends Component{
    constructor() {
        super();
        this.state = {
            coursename: [],
            course:''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    // componentDidMount() {
    //     axios.get('http://localhost:3002/students').then(data => {
    //         this.setState({
    //             courses: data.data.data
    //         })
    //         console.log(data);
    //     })
    // }
    handleSubmit(courseName) {
        if(this.validateFields()){
            const obj = {
                coursename:courseName
            };
            axios.get('http://localhost:3002/students/:coursename', obj).then(
                data => {
                    alert('Successfull' + JSON.stringify(data.data));
                    if (data) {
                        // return data.json();
                    } else {
                        return data.status(500).json({message: 'Error'});
                    }
                }
            )}
    }

    render() {
        return(
            <div>
                <h2 style={{marginTop:20}}>Notifications</h2>
                <table className="table table-striped" style={{marginTop:20}}>
                    <thead>
                    </thead>
                    <tbody>
                    {
                        // this.state.courses.map(course=>{
                        //     return <tr key={course._id}>
                        //         <td>{course.name}</td>
                        //         <td>
                        //             <button onClick={this.handleSubmit.bind(this,course.name)}>Accept Course</button>
                        //         </td>
                        //     </tr>
                        // })
                        <tr>
                            <td>Application framework</td>
                            <td>
                                <button className="btn btn-info">Accept Course</button>
                            </td>
                        </tr>

                    }
                    </tbody>
                </table>
            </div>
        );
    }
}