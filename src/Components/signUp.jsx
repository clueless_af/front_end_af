import React, {Component} from 'react';
import '../StyleSheets/signup.css'
import axios from "axios";
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import ReactDOM from 'react-dom';
import Login from './login';
import Home from '../App';

export default class SignUpComponent extends Component {

    constructor() {
        super();
        this.state = {
            name: '',
            nic: '',
            birthday: '',
            mobileNo: '',
            email: '',
            password: '',
            userlevel: '',
            confrmPw: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        if(this.validateFields()){
        const obj = {
            name: this.state.name,
            nic: this.state.nic,
            birthday: this.state.birthday,
            email: this.state.email,
            mobileNo: this.state.mobileNo,
            password: this.state.password

        };
        axios.post('http://localhost:3002/students', obj).then(
            data => {
                alert('Successfull' + JSON.stringify(data.data));
                if (data) {
                    this.loginpage();
                    // return data.json();
                } else {
                    return data.status(500).json({message: 'Error'});
                }
            }
        )}
    }

    loginpage = () => {
        ReactDOM.render(<Home/>, document.getElementById('root'));
    };

     validEmail=(email)=> {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    validateFields = () => {
        if (this.state.name === '' || this.state.nic === '' || this.state.mobileNo === '' || this.state.email === '' ||
            this.state.password === '' || this.state.birthday === '' || this.state.confrmPw === '') {
            alert("One or more fields out field are empty!!")
            return false
        }
        if (this.state.password !== this.state.confrmPw) {
            alert("Password and Confirm password fields are not equal!!")
            return false
        }
        if(this.state.mobileNo.length !==10){
            alert("Invalid contact number!! ")
            return false
        }
        if(this.state.nic.length !==10 || this.state.nic.charAt(9)!== 'v'|| this.state.nic.charAt(9)!== 'v'){
            alert("invalid Nic")
            return false
        }
        if(!this.validEmail(this.state.email)){
            alert("invalid email");
            return false
        }
        return true
    };

    render() {
        return (
            <div className="signUpDev" style={{marginTop: 0}}>
                <div>
                <h1>InfoWeb</h1>
                    <button onClick={this.loginpage} className="btn btn-primary" style={{marginLeft:1000} }>Login</button>
                </div>
                <h3 style={{marginTop: 20}} className="text-primary">SIGNUP</h3>
                <form className="loginForm" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Name</label>
                        <input type="text" className="form-control"
                               name="name"
                               value={this.state.name}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>NIC</label>
                        <input type="text" className="form-control"
                               name="nic"
                               value={this.state.nic}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Birthday</label>
                        <input type="date" className="form-control"
                               name="birthday"
                               value={this.state.birthday}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>E-mail</label>
                        <input type="email" className="form-control"
                               name="email"
                               value={this.state.email}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Mobile No</label>
                        <input type="number" className="form-control"
                               name="mobileNo"
                               value={this.state.mobileNo}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control"
                               name="password"
                               value={this.state.password}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Confirm Password </label>
                        <input type="password" className="form-control"
                               value={this.state.confrmPw}
                               onChange={this.handleInputChange}
                               name="confrmPw"
                        />
                    </div>

                    <div className="form-group">
                        <input type="submit" value="SignUp" className="btn btn-primary"/>
                    </div>

                </form>
            </div>
        );
    }
}