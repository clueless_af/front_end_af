import React, {Component} from 'react';
import '../StyleSheets/login.css'
import axios from 'axios'
import ReactDOM from "react-dom";
import Home from "./signUp";
import StudentHome from './studentHome';
import InstructorHome from './instructorHome';
import AdminHome from '../Admin/App';

export default class LoginComponent extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            name: '',
            password: '',
            userLevel: '',
            nic: '',
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.baseState = this.state;
    }

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const obj = {
            nic: this.state.nic,
            password: this.state.password
        };
        axios.post('http://localhost:3002/students/login', obj).then(
            data => {
                console.log(data);
                if (data.status === 200 && data.data.userLevel === 3) {
                    localStorage.setItem('sis-user', JSON.stringify(data));
                    alert('Successful' + JSON.stringify(data.data));
                    this.toStudentHome();
                } else if (data.status === 200 && data.data.userLevel === 2) {
                    localStorage.setItem('sis-user', JSON.stringify(data));
                    alert('Successful' + JSON.stringify(data.data));
                    this.toInstructorHome();
                } else if (data.status === 200 && data.data.userLevel === 1) {
                    localStorage.setItem('sis-user', JSON.stringify(data));
                    alert('Successful' + JSON.stringify(data.data));
                    this.toAdminHome();
                } else if (data.status !== 200) {
                    alert('Unsuccessful');
                    this.setState({
                        name: '',
                        password: '',
                        userLevel: '',
                        nic: '',
                    })
                } else {
                    alert('Unsuccessful');
                }
            }
        ).catch(err => {
            alert('Unsuccessful');
            console.log(err);
        })
    }

    toStudentHome = () => {
        ReactDOM.render(<StudentHome/>, document.getElementById('root'));
    };

    toInstructorHome = () => {
        ReactDOM.render(<InstructorHome/>, document.getElementById('root'));
    };

    toAdminHome = () => {
        ReactDOM.render(<AdminHome/>, document.getElementById('root'));
    };

    //directing to signUp
    toSignUp = () => {
        ReactDOM.render(<Home/>, document.getElementById('root'));
    };


    render() {
        return (
            <div className="logonDev" style={{marginTop: 20}}>
                <h2 style={{marginTop: 20}} className="text-primary">LOGIN</h2>
                <form className="loginForm" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>User Name</label>
                        <input type="text"
                               className="form-control"
                               name="nic"
                               value={this.state.nic}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control"
                               name="password"
                               value={this.state.password}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <input type="submit" value="LOGIN" className="btn btn-primary"/>
                    </div>
                    <div className="form-group">
                        <button className=" btn btn-link" onClick={this.toSignUp}>Login to Continue <br/>If You have not
                            sign up click here!!
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}