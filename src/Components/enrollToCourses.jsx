import React, {Component} from 'react';
import '../StyleSheets/viewAssignments.css'
import Axios from 'axios';

export default class EnrollCoursesComponent extends Component{
    constructor() {
        super();
        this.state = {
            courses: [],
            nic:''
        }
    }
    componentDidMount() {
        var user = localStorage.getItem('sis-user')
        if (user) {
            user = JSON.parse(user)
            this.setState({
                nic:user.data.nic
            })
        }
        Axios.get('http://localhost:3002/courses/').then(data => {
            this.setState({
                courses: data.data
            })
            console.log(data);
        })
    }

    handleSubmit(e) {
        e.preventDefault();
            const obj = {
                courses: this.state.courses,
                nic: this.state.nic

            };
            Axios.put('http://localhost:3002/students/'+ this.state.nic,obj).then(data => {
                if(data){
                    alert("enrolled");
                }
                console.log(data);
            })
    }
    render() {
        return(
            <div>
                <h2 style={{marginTop:20}}>Courses</h2>
                <table className="table table-striped" style={{marginTop:20}}>
                    <thead>
                    <th>Course name</th>
                    <th>Course code</th>
                    <th>Duration</th>
                    </thead>
                    <tbody>
                    {
                        this.state.courses.map(sub=>{
                            return <tr key={sub._id}>
                                <td>
                                    {sub.cName}
                                </td>
                                <td>
                                    {sub.cCode}
                                </td>
                                <td>
                                    {sub.duration}
                                </td>
                                <td>
                                    {/*<button className="btn btn-info" onClick={this.handleSubmit}>Enroll</button>*/}
                                    <button className="btn btn-info" >Enroll</button>
                                </td>
                            </tr>
                        })

                    }
                    </tbody>
                </table>
            </div>
        );
    }
}