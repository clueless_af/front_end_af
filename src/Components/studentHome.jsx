import React, {Component} from 'react';
import '../StyleSheets/studentHome.css';

import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import Courses from './viewCourses';
import Marks from './viewMarksStudent';
import Assignmets from './viewAssignmentsStudent';
import Notifications from './notificationsStudent';
import AddSubmission from './submitAssignment';
import EnrollCourses from './enrollToCourses';
import Login from './login';
import ReactDOM from "react-dom";

export default class StudentHome extends Component {
    constructor() {
        super();
        this.state = {
            nic:''
        }
    }

    componentDidMount() {
        var user = localStorage.getItem('sis-user')
        if (user) {
            user = JSON.parse(user)
            this.setState({
                nic:user.data.nic
            })
        }}

    logout = () => {
        localStorage.setItem('sis-user', '');
        ReactDOM.render(<Login/>, document.getElementById('root'));
    };
    render() {
        return (
            <div className="studentHome">
                <h1 style={{marginBottom:0}}>InfoWeb</h1>
                <div style={{marginTop:0}} >
                <Router>
                    <nav className="navbar navbar-expand-sm bg-dark navbar-dark " style={{marginTop:0}}>
                        <a className="navbar-brand" href="#">Student</a>

                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link className="nav-link" to="/courses"> My Courses</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/allcourses"> Enroll to Courses</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/marks">Marks</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/assignments">Assignments</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/notifications">Notifications</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/addSubmission">Add Submission</Link>
                            </li>
                            <li className="nav-item">
                               <button className="btn btn-default" style={{paddingLeft:300,color:"white"}} onClick={this.logout}>LOGOUT</button>
                            </li>
                        </ul>
                    </nav>
                    <div className='stHomeComp'>
                        <Switch>
                            <Route path="/courses" component={Courses}></Route>
                            <Route path="/marks" component={Marks}></Route>
                            <Route path="/assignments" component={Assignmets}></Route>
                            <Route path="/notifications" component={Notifications}></Route>
                            <Route path="/addSubmission" component={AddSubmission}></Route>
                            <Route path="/allcourses" component={EnrollCourses}></Route>
                        </Switch>
                    </div>
                </Router>
                </div>
            </div>
        );
    }
}