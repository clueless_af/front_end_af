import React, {Component} from 'react';
import '../StyleSheets/instructorHome.css';

import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import Courses from './viewCourses';
import Marks from './addMarks';
import Assignmets from './ViewAssignments';
import Notifications from './notifications';
import AddAssignment from './addAssignment';
import ReactDOM from "react-dom";
import Login from "./login";

export default class InstructorHome extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            nic: '',
        };
    }

    componentDidMount() {
        var user = localStorage.getItem('sis-user')
        if (user) {
            user = JSON.parse(user)
            this.setState({
                nic: user.data.nic
            })
        }
    }

    logout = () => {
        ReactDOM.render(<Login/>, document.getElementById('root'));
    };

    render() {
        return (
            <div className="instructorHome">
                <h1 style={{marginBottom: 0}}>InfoWeb</h1>
                <div style={{marginTop: 0}}>
                    <Router>
                        <nav className="navbar navbar-expand-sm bg-dark navbar-dark " style={{marginTop: 0}}>
                            <a className="navbar-brand" href="#">Instructor</a>

                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <Link className="nav-link" to="/courses">Courses</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/marks">Marks</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/addAssignment">Add Assignment</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/assignments">Assignments</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="/notifications">Notifications</Link>
                                </li>
                                <li className="nav-item">
                                    <button className="btn btn-default" style={{paddingLeft: 300, color: "white"}}
                                            onClick={this.logout}>LOGOUT
                                    </button>
                                </li>
                            </ul>
                        </nav>
                        <div className="insHomeComp">
                            <Switch>
                                <Route path="/courses" component={Courses}></Route>
                                <Route path="/marks" component={Marks}></Route>
                                <Route path="/assignments" component={Assignmets}></Route>
                                <Route path="/notifications" component={Notifications}></Route>
                                <Route path="/addAssignment" component={AddAssignment}></Route>
                            </Switch>
                        </div>
                    </Router>
                </div>
            </div>
        );
    }
}