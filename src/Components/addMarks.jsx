import React, {Component} from 'react';
import '../StyleSheets/viewAssignments.css'
import Axios from "axios";
import ReactDOM from "react-dom";
import Assignments from "./ViewAssignments";

export default class ViewCoursesComponent extends Component {

    constructor() {
        super();
        this.state = {
            students: [],
            nic: ''
        }
    }

    toAssignment = () => {
        ReactDOM.render(<Assignments/>, document.getElementById('root'));
    };

    componentDidMount() {
        var currentAssign = localStorage.getItem('current-assign');
        var user = localStorage.getItem('sis-user');
        if (user) {
            user = JSON.parse(user);
            this.setState({
                nic: user.data.nic
            })
        }
        Axios.get('http://localhost:3002/students/').then(data => {
            this.setState({
                students: data.data.data
            })
            console.log(data);
        })
    }

    render() {
        return (
            <div>
                <div>
                    <h2 style={{marginTop: 20}}>Add Marks</h2>
                </div>
                <form className="addMarks" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <div className="row">
                            <div className="col-sm-4">
                                <label>Assignment Name</label>
                            </div>
                            <div className="col-sm-4">
                                <input type="text"
                                       className="form-control "
                                       name="Assignments"
                                /></div>
                        </div>
                    </div>
                </form>
                <table className="table table-striped" style={{marginTop: 20}}>
                    <thead>
                    <th>Student ID</th>
                    <th>Marks</th>
                    </thead>
                    <tbody>
                    {
                        this.state.students.map(sub => {
                            return <tr key={sub.nic}>
                                <td>
                                    {sub.nic}
                                </td>
                                <td>
                                    <input type="text" className="form-control"
                                    />
                                </td>
                            </tr>
                        })
                    }
                    </tbody>
                </table>
                <button className="btn btn-primary">Submit</button>
            </div>
        );
    }
}