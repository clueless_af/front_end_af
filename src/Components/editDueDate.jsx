import React, {Component} from 'react';
import '../StyleSheets/addAssignment.css'
import axios from "axios";

export default class EditDueDates extends Component {
    constructor() {
        super();
        this.state = {
            id:'',
            assignmentname: '',
            coursename: '',
            description: '',
            due_dates: '',
            submittedBy: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        var usr = localStorage.getItem('current-assign');
        if (usr) {
            usr = JSON.parse(usr)
            this.setState({
                id: usr.data
            })
        }
        axios.get('http://localhost:3002/assignments'+usr.data).then(response => {
            this.setState({
                assignments: response.data
            });
            console.log("jsfhg" + response);
        })
    }

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const obj = {
            assignmentname: this.state.assignmentname,
            coursename: this.state.coursename,
            description: this.state.birthday,
            due_dates: this.state.due_dates,
            submittedBy: this.state.submittedBy

        };
        axios.put('http://localhost:3002/assignments'+this.state.id, obj).then(
            data => {
                alert('Successfull' + JSON.stringify(data.data));
                if (data) {
                    console.log(data);
                    this.setState({
                        assignmentname: '',
                        coursename: '',
                        description: '',
                        due_dates: '',
                        submittedBy: ''
                    })
                } else {
                    return data.status(500).json({message: 'Error'});
                }
            }
        )
    }

    render() {
        return (
            <div className="addAssignment" style={{marginTop: 0}}>

                <h2 style={{marginTop: 0}} className="text-primary">Add Assignment</h2>
                <form className="assignmentForm" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Assignment Name</label>
                        <input type="text" className="form-control"
                               name="assignmentname"
                               value={this.state.assignmentname}
                               onChange={this.handleInputChange}
                        />
                    </div>


                    <div className="form-group">
                        <label>Course</label>
                        <input type="text" className="form-control"
                               name="coursename"
                               value={this.state.coursename}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Description</label>
                        <textarea className="form-control"
                                  name="description"
                                  value={this.state.description}
                                  onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Due Date</label>
                        <input type="date" className="form-control"
                               name="due_dates"
                               value={this.state.due_dates}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>NIC</label>
                        <input type="text" className="form-control"
                               name="submittedBy"
                               value={this.state.submittedBy}
                               onChange={this.handleInputChange}
                        />
                    </div>

                    <div className="form-group">
                        <input type="submit" value="ADD" className="btn btn-primary"/>
                    </div>

                </form>
            </div>
        );
    }
}