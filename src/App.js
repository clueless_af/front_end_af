import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import Login from './Components/login';


function App() {
    return (
        <div className="App">
            <h1>InfoWeb</h1>
            <Login/>
        </div>
    );
}

export default App;
